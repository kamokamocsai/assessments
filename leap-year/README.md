###Description
Given a year, report if it is a leap year.

The tricky thing here is that a leap year in the Gregorian calendar occurs:

```
1 on every year that is evenly divisible by 4
2 except every year that is evenly divisible by 100
3 unless the year is also evenly divisible by 400
```
For example, 1997 is not a leap year, but 1996 is. 1900 is not a leap year, but 2000 is.

###Guidelines
- Fork this project.
- Create a plain solution on your language
- If your language provides a method in the standard library that does this look-up, pretend it doesn't exist and implement it yourself.
- Write tests
- Commit the important milestones and not just the final result
