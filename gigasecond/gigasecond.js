'use strict';

function gigasecondCalculator(date){
  let dateTemp = date.split('.');
  let oneDaySeconds = 24 * 60 * 60;
  let gigaSecond = 1000000000;
  let oneYearSeconds = 365.25 * oneDaySeconds;
  
  let year = gigaSecond / oneYearSeconds; // How many year is one gigasecond.
  
  let yearToString = year.toString().split('.');
  let restOne = '0.' + yearToString[1]
  let restMonth = restOne * 12;

  let monthToString = restMonth.toString().split('.');
  let restTwo = '0.' + monthToString[1];
  let restWeeks = restTwo * 4;

  let weeksToString = restWeeks.toString().split('.');
  let restWeekOther = '0.' + weeksToString[1];
  let restDays = restTwo * 7;

  let daysToString = restDays.toString().split('.');
  let restThree = '0.' + daysToString[1];
  let restHour = restThree * 24;

  let hourToString = restHour.toString().split('.');
  let restFour = '0.' + hourToString[1];
  let restMinutes = restFour * 60;
  
  let minutesToString = restMinutes.toString().split('.');
  let restFive = '0.' + minutesToString[1];
  let restSeconds = restFive * 60;

  console.log('1 gigasec = ' + yearToString[0] + ' year, ' +  monthToString[0] + ' month, ' + weeksToString[0]
  + ' weeks, ' + daysToString[0] + ' days, ' + hourToString[0] + ' hours, ' + minutesToString[0] + ' minutes and '
  + restSeconds.toString().split('.')[0] + ' seconds.')

  let solutionYear = Number(dateTemp[0]) + Number(yearToString[0]);
  let solutionMonth = Number(dateTemp[1]) + Number(monthToString[0]);
  let solutionDays = Number(dateTemp[2]) + ( Number(weeksToString[0]) * 7 ) + Number(daysToString[0])
  
  dateConverter(solutionYear, solutionMonth, solutionDays)
};

function dateConverter(solutionYear, solutionMonth, solutionDays){
  if (solutionMonth > 12){
    solutionMonth -= 12;
    solutionYear += 1;
  };

  if (solutionMonth === 2 && solutionDays > 28){
    solutionDays -= 28;
    solutionMonth += 1;
  } else if (solutionMonth === 4 || solutionMonth === 6 || solutionMonth === 9 || solutionMonth === 11 && solutionDays > 30){
    solutionDays -= 30;
    solutionMonth += 1;
  } else {
    if(solutionDays > 31){
      solutionDays -= 31;
      solutionMonth += 1;
    };
  };

  let finalDate = [];
  finalDate.push(solutionYear);
  finalDate.push(solutionMonth);
  finalDate.push(solutionDays);
  
  let finalSolution = finalDate[0].toString() + '.' + finalDate[1].toString() + '.' + finalDate[2].toString();
  console.log(finalSolution);  
};

gigasecondCalculator(1983.09.26)